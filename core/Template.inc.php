<?php
namespace Core;

class Template
{
    protected $FileName;
    protected $ParamsArray;
    public function __construct($fileName)
    {
        $this->FileName = $fileName;
        $this->ParamsArray = [];
    }
    public function AddParams($arr)
    {
        foreach ($arr as $key => $value)
            $this->AddParam($key, $value);
    }
    public function AddParam($name, $value)
    {
        $this->ParamsArray[$name] = $value;
    }
    public function GetParam($name)
    {
        return $this->ParamsArray[$name];
    }
    public function Display()
    {
        $html = $this->GetHTML();
        echo $html;
        return $html;
    }
    public function GetHTML()
    {
        ob_start();
        extract($this->ParamsArray);
        include($this->FileName);
        $html = ob_get_contents();
        ob_end_clean();
        return $html;
    }
}