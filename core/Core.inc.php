<?php
namespace Core;

class Core
{
    public static $DB;
    public static $IndexTPL;
    public static function Init()
    {
        session_start();
        self::$DB = new MysqlDB(DATABASE_SERVER, DATABASE_NAME, DATABASE_USER, DATABASE_PASSWORD);
        self::$IndexTPL = new namespace\Template("app/views/index.tpl");
    }
    public static function Run()
    {
        $route = $_GET['r'];
        $routeParts = explode('/', $route);
        if (count($routeParts) > 0)
        {
            $className = '\\App\\Controllers\\'.ucfirst(array_shift($routeParts));
            if (count($routeParts) > 0)
            {
                $methodName = ucfirst(array_shift($routeParts));
            } else
                $methodName = "Index";
        }
        if (class_exists($className)) {
            $controller = new $className();
            if (method_exists($controller, $methodName)) {
                $vars = $controller->$methodName();
                self::$IndexTPL->AddParams($vars);
            } else
            {
                // Gen 404 Error
            }
        } else
        {
            // Gen 404 Error
        }
    }
    public static function Done()
    {
        self::$IndexTPL->Display();
    }
}
?>