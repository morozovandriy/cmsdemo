<?php
require_once('config/database.inc.php');
error_reporting(E_ALL);
ini_set('display_errors', 'On');

function core_classes_autoload($className)
{
    $classNameParts = explode("\\", $className);
    $folderName = strtolower($classNameParts[0]);
    $fileName = $classNameParts[1].".inc.php";
    $fullPath = $folderName."/".$fileName;
    if (is_file($fullPath))
        include($fullPath);
}

function controllers_autoload($className)
{
    $classNameParts = explode("\\", $className);
    $fileName = array_pop($classNameParts).'.inc.php';
    $folderName = strtolower(implode("/", $classNameParts));
    $fullPath = $folderName."/".$fileName;
    if (is_file($fullPath))
        include($fullPath);
}


spl_autoload_register("core_classes_autoload");
spl_autoload_register("controllers_autoload");

Core\Core::Init();
Core\Core::Run();
Core\Core::Done();

/*
$tpl = new Core\Template("app/views/index.tpl");
$tpl->AddParam("Title", "Gdfgdfgdfgdfgdf dfg dfg dfg d");
$tplNews = new Core\Template("app/views/news.tpl");
$tplNews->AddParam("NewsBlock", "<h2>!!!</h2>");
$tpl->AddParam("Content", $tplNews->GetHTML());
$html = $tpl->Display();*/
//$obj = new Core\Core1();

/**/